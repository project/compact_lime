<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $language->language; ?>" xml:lang="<?php echo $language->language; ?>">

  <head>
    <title><?php echo $head_title ?></title>
    <?php echo $head ?>
    <?php echo $styles ?>
    <?php echo $scripts ?>
    <!--[if IE 6]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie6.css" type="text/css" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie7.css" type="text/css" media="screen" /><![endif]-->
  </head>

  <body<?php echo compact_lime_body_class($left, $right); ?>>
  <!--noindex-->
    <div class="Header">
      <div class="right_bg"></div>
      <div id="headercontent" class="column">
        <div class="logo">
          <?php
            if (!empty($site_name)) echo '<h1 class="logo-name"><a href="' . check_url($base_path) . '" title = "' . $site_name . '">' . $site_name . '</a></h1>';
            if (!empty($site_slogan)) echo '<div class="logo-text">' . $site_slogan . '</div>';
          ?>
        </div>
      </div>
    </div>
    <?php if (!empty($navigation)): ?>
      <div class="nav">
        <div class="l"></div>
        <div class="r"></div>
        <div class="column">
          <?php echo $navigation; ?>
        </div>
      </div>
    <?php endif; ?>

    <div id="page">
      <div id="container" class="column">
        <div id="main">
          <?php if ($content_top): ?>
            <div id="content-top">
              <?php echo $content_top; ?>
            </div>
          <?php endif; ?>
          <div id="content-padding">
            <?php if ($show_messages) echo $messages; ?>
            <!--endnoindex-->
            <?php if ($title): ?><h2 class="title"><?php echo $title; ?></h2><?php endif; ?>
            <!--noindex-->
            <?php echo $breadcrumb; ?>
            <?php if ($tabs): ?><div class="tabs"><?php echo $tabs; ?></div><?php endif; ?>
            <?php echo $help ?>
            <!--endnoindex-->
            <?php echo $content; ?>
            <!--noindex-->
            <?php echo $feed_icons; ?>
          </div>
          <?php if ($content_bottom): ?>
            <div id="content-bottom">
              <?php echo $content_bottom; ?>
            </div>
          <?php endif; ?>
        </div>
        <?php if ($right): ?>
          <div id="sidebar-right" class="sidebar">
            <?php echo $right; ?>
          </div>
        <?php endif; ?>
        <div class="clear-block"></div>
      </div>
    </div>

    <?php if ($footer): ?>
      <div id="footer">
        <div class="column">
          <?php echo $footer; ?>
        </div>
      </div>
    <?php endif; ?>
    <div id="footer-message">
      <p class="page-footer"><?php echo $footer_message; ?></p>
    </div>
    <!--endnoindex-->
    <?php echo $closure; ?>
  </body>
</html>
